import React from 'react';

import Pagination from '../components/Pagination';
import LinksTable from '../components/LinksTable';
import {CFG_HTTP} from "../cfg/cfg_http";
import {UtilsApi} from "../utils/utils_api";

class LinksContainer extends React.Component {
    handlePageChange = (pageNumber) => {
        this.fetchLinks(pageNumber);
    };

    fetchLinks = (currentPage = 0) => {
        let sendData = {page: currentPage, itemPerPage: 5};

        UtilsApi
            .get(CFG_HTTP.URL_LINKS, sendData)
            .then((links) => {
                console.log(links);
                this.setState({
                    links: links.links,
                    pagesLimit: links.maxPage,
                    currentPage: links.currentPage
                });
            });
    };

    componentDidMount() {
        this.fetchLinks();
    }

    constructor() {
        super();

        this.state = {
            links: [],
            pagesLimit: 0,
            currentPage: 0
        };
    }

    render() {
        return (
            <React.Fragment>
                <Pagination onPageChange={this.handlePageChange}
                            currentPage={this.state.currentPage}
                            pagesLimit={this.state.pagesLimit}
                            />
                <LinksTable links={this.state.links}/>
            </React.Fragment>
        );
    }
}

export default LinksContainer;